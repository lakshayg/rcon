#ifndef _MOTION_H_
#define _MOTION_H_

#define motion_score 10
#include <opencv2/opencv.hpp>
using namespace cv;

void motionScore(Mat prevImg, Mat img, vector<Point> *pts, vector<int> *score);


#endif
