/*
  Author: Saksham Sharma
*/

#include "libfreenect.hpp"
#include <pthread.h>
#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <string>

using namespace cv;

#ifndef _KINECT_H_
#define _KINECT_H_

class myMutex {
 public:
  myMutex();
  void lock();
  void unlock();
 public:
  pthread_mutex_t m_mutex;
};


class MyFreenectDevice : 
public Freenect::FreenectDevice {
 public:
  bool m_new_rgb_frame;
  bool m_new_depth_frame;
  std::vector<uint8_t> m_buffer_depth;
  std::vector<uint8_t> m_buffer_rgb;
  std::vector<uint16_t> m_gamma;
  Mat depthMat;
  Mat rgbMat;
  Mat ownMat;
  myMutex m_rgb_mutex;
  myMutex m_depth_mutex;
		

 public:
 MyFreenectDevice(freenect_context *_ctx, int _index): Freenect::FreenectDevice(_ctx, _index),
    m_buffer_depth(FREENECT_DEPTH_11BIT),
    m_buffer_rgb(FREENECT_VIDEO_RGB),
    m_gamma(2048),
    m_new_rgb_frame(false),
    m_new_depth_frame(false),
    depthMat(Size(640,480),CV_16UC1),
    rgbMat(Size(640,480), CV_8UC3, Scalar(0)),
    ownMat(Size(640,480),CV_8UC3,Scalar(0))
      {	
	for( unsigned int i = 0 ; i < 2048 ; i++) {
	  float v = i/2048.0;
	  v = std::pow(v, 3)* 6;
	  m_gamma[i] = v*6*256;
	}
      };
		
  void VideoCallback(void* _rgb, uint32_t timestamp);
  void DepthCallback(void* _depth, uint32_t timestamp);
  bool getVideo(Mat& output);		
  bool getDepth(Mat& output);
};

extern double THETA;
extern double focal;

#endif
