#ifndef _CLEAN_H_
#define _CLEAN_H_

#include <opencv2/opencv.hpp>
#include <iostream>
using namespace std;
using namespace cv;
void clean(vector<Point> *candidates, vector<int> *score, int min_score, Mat rgb, bool draw);

#endif
