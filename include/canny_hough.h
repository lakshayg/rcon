#ifndef _CANNY_HOUGH_H_
#define _CANNY_HOUGH_H_

#include <opencv2/opencv.hpp>

using namespace cv;

void cannyHoughScore(Mat gray, vector<Point> *points, vector<int> *score);

#endif
