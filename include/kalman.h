#ifndef _KALMAN_H_
#define _KALMAN_H_

#include <armadillo>
#include "debug.h"
#include "controller.h"
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace std;

#define reInitKalman(x) A_##x = arma::mat(2,2,arma::fill::eye);		\
  P_##x = arma::mat(2,2,arma::fill::eye);				\
  Q_##x = arma::mat(2,2,arma::fill::zeros);				\
  B_##x = arma::mat(2,2,arma::fill::zeros);				\
  H_##x = arma::mat(2,2,arma::fill::eye);				\
  R_##x = arma::mat(2,2,arma::fill::eye);				\
  X_##x = arma::mat(2,1,arma::fill::zeros);				\
  U_##x = arma::mat(2,1,arma::fill::zeros);				\
  A_##x << 1 << (dt-k*dt*dt/(2*m)) << arma::endr << 0 << (1 - k*dt/m) << arma::endr; \
  U_##x << -0.5*g_##x*dt*dt << arma::endr << -g_##x*dt << arma::endr;	\
  P_##x = 1000*P_##x;\
  Q_##x = 0.1 *Q_##x;\
  R_##x = 0.2 *R_##x;

extern float k;
extern float m; 
extern float g_x;
extern float g_y;
extern float g_z;
extern float dt;

extern double x;
extern double y;
extern double z;
extern double v_x;
extern double v_y;
extern double v_z;
extern double old_x;
extern double old_y;
extern double old_z;
extern bool Kalman_flag;

extern arma::mat A_x;
extern arma::mat P_x;
extern arma::mat Q_x;
extern arma::mat B_x;
extern arma::mat H_x;
extern arma::mat R_x;
extern arma::mat X_x;
extern arma::mat U_x;

extern arma::mat A_y;
extern arma::mat P_y;
extern arma::mat Q_y;
extern arma::mat B_y;
extern arma::mat H_y;
extern arma::mat R_y;
extern arma::mat X_y;
extern arma::mat U_y;

extern arma::mat A_z;
extern arma::mat P_z;
extern arma::mat Q_z;
extern arma::mat B_z;
extern arma::mat H_z;
extern arma::mat R_z;
extern arma::mat X_z;
extern arma::mat U_z;

void Kalman_calc(double x, double v_x, double y, double v_y, double z, double v_z);
void resetKalman();
void predict();
int mini(int a, int b);
int maxi(int a, int b);
double tanF(double x);
double get_dt();

#endif
