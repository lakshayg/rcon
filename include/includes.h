#ifndef _INCLUDES_H_
#define _INCLUDES_H_

#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>
#include <cmath>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <stdio.h>
#include <armadillo>

#include "glhelper.h"
#include "util.h"
#include "kinect.h"
#include "kalman.h"
#include "debug.h"
#include "controller.h"
#include "cluster.h"
#include "canny_hough.h"
#include "clean.h"
#include "motion.h"
#include "optflow.h"
#include "color.h"
#include "align.h"

#endif
