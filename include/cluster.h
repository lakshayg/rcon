#ifndef _CLUSTER_H_
#define _CLUSTER_H_

#include <opencv2/opencv.hpp>

#define cluster_score 0
#define cluster_penalty 50
#define roi_size 10

using namespace cv;

void clusterScore(vector<Point> *keypoints, vector<int> *score);

#endif
