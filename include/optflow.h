#ifndef _OPTFLOW_H_
#define _OPTFLOW_H_

#include <opencv2/opencv.hpp>


using namespace cv;

void optFlowScore(Mat prevImg, Mat img, vector<Point> *points, vector<int> *score);

#endif
