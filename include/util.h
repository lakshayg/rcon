/*
 * Author: Saksham Sharma
 */

#ifndef _UTIL_H_
#define _UTIL_H_

#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>
#include <string>

using namespace cv;

// Custom Functions. You can add new ones here.
double getDist(int pDepth);												// Returns the distance of the object, given it's depth;
double getDistAccurate(int pDepth);										// Returns distance using tan and not tanf.
double getFPS();														// Returns the FPS in double.
void printFPS(Mat& img);												// Prints FPS at the lower edge of the screen.
void print_int_on_screen(Mat& img, int integer, int x, int y);			//Prints the given integer on the given image at the given coordinates.
void print_double_on_screen(Mat& img, double doubler, int x, int y);	//Prints the given double number on the given image at the given coordinates.

#endif
