/*
 *	Controller commands library.
 *	Contains the characters sent to arduino for actions.
 *	Also contains the integer bindings of the PS3 controller.
 */

#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

#include <iostream>
#include <fcntl.h>
#include <pthread.h>
#include <math.h>
#include <linux/joystick.h>
#include <vector>
#include <unistd.h>

#include "kalman.h"
#include "debug.h"

#define JOYSTICK_DEV "/dev/input/js0"

#define LEFT_170 "j"
#define RIGHT_170 "l"
#define FRONT_170 "i"
#define BACK_170 "k"

#define LEFT_210 "a"
#define RIGHT_210 "d"
#define FRONT_210 "w"
#define BACK_210 "s"

#define LEFT_250 "A"
#define RIGHT_250 "D"
#define FRONT_250 "W"
#define BACK_250 "S"

#define LEFT_KIN "a"
#define RIGHT_KIN "d"
#define FRONT_KIN "w"
#define BACK_KIN "s"

#define CLK_SLOW "v"
#define CLK_FAST "b"
#define ACLK_SLOW "c"
#define ACLK_FAST "x"

#define HR_STOP "5"
#define VR_STOP "6"
#define PAUSE "o"		// puts all motors on low
#define STOP "0"		// all terminals on high
#define STAY "1"		// all terminals on high, then all terminals on low after 200 milliseconds.
#define HIT "h"			// to hit the shuttle

#define KEY_lt 8		// left trigger
#define KEY_rt 9		// right trigger
#define KEY_start 3		// start button

#define KEY_x 14		// cross
#define KEY_t 12		// triangle
#define KEY_o 13		// circle
#define KEY_s 15		// square

struct joystick_position {
  float theta, r, x, y;
};

struct joystick_state {
  std::vector<signed short> button;
  std::vector<signed short> axis;
};

class cJoystick {
 private:
  pthread_t thread;
  bool active;
  int joystick_fd;
  js_event *joystick_ev;
  joystick_state *joystick_st;
  __u32 version;
  __u8 axes;
  __u8 buttons;
  char name[256];

 protected:
 public:
  cJoystick();
  ~cJoystick();
  static void* loop(void* obj);
  void readEv();
  joystick_position joystickPosition(int n);
  bool buttonPressed(int n);
};


extern bool xInput;             // Whether controller is giving command in x axis at the moment.
extern bool yInput;             // Whether controller is giving command in y axis at the moment.
extern bool Buttons[19];

extern cJoystick js;
extern joystick_position jp[2];

void processInput();
void getJoyMotion();

#endif