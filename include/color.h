#ifndef _COLOR_H_
#define _COLOR_H_

#include <opencv2/opencv.hpp>

using namespace cv;

void colorScore(Mat gray, vector<Point> *points, vector<int> *score);

#endif
