/*
 * Author: Saksham Sharma
 */

#include "../include/util.h"

using namespace cv;
using namespace std;

//============================ Custom Functions ===============================//

double getDist(int pDepth) {
  return (-1)* 7.5 / ( tanf(0.0002157 * pDepth - 0.2356) );
}

double getDistAccurate(int pDepth) {
  return (-1)* 7.5 / ( tan(0.0002157 * pDepth - 0.2356) );
}

double getFPS() {
  static double t = 0;
  t = ((double)getTickCount() - t)/getTickFrequency();
  double p = 1.0/t;
  t = (double)getTickCount();
  return p;
}

void printFPS(Mat& img) {
  static double t = 0;
  t = ((double)getTickCount() - t)/getTickFrequency();
  t = 1.0/t;

  stringstream FPS;
  FPS << t;
  putText(img, FPS.str(), Point(260,460), 1, 2, Scalar(0,255,255), 2, 2, 0);

  t = (double)getTickCount();
}

void print_int_on_screen(Mat& img, int integer, int x, int y) {
  stringstream data;
  data << integer;
  putText(img, data.str(), Point(x,y), 1, 2, Scalar(0,255,255), 2, 2, 0);
}

void print_double_on_screen(Mat& img, double doubler, int x, int y) {
  stringstream data;
  data << doubler;
  putText(img, data.str(), Point(x,y), 1, 2, Scalar(0,255,255), 2, 2, 0);
}
