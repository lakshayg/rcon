#include "../include/clean.h"

void clean(vector<Point> *candidates, vector<int> *score, int min_score, Mat rgb, bool draw) {

  Point maxLoc;
  double maxVal;
  minMaxLoc(*score, NULL, &maxVal, NULL, &maxLoc);

  if (min_score == -1)
    min_score = (int)maxVal;

  vector<Point>::iterator cit;
  vector<int>::iterator sit;
  vector<Point>::iterator ctmp;
  vector<int>::iterator stmp;  
  for(cit = candidates->begin(), sit = score->begin(); cit != candidates->end(); ++cit, ++sit) {
    if (*sit < min_score) {
      ctmp = cit;
      stmp = sit;
      cit--; sit--;
      candidates->erase(ctmp);
      score->erase(stmp);
    }
  }
  
  if (draw) {
    stringstream ss;
    ss << maxVal;
    for (int i=0; i<candidates->size(); i++){
      if (score->at(i) < maxVal)
        continue;
      circle(rgb, candidates->at(i), 3, Scalar(255, 0, 255), 4);
      putText(rgb, ss.str(), candidates->at(i), 3, 1, Scalar(255, 0, 255), 2, 8);
    }
  }
}
