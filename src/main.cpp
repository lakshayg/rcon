#include "../include/includes.h"

using namespace std;
using namespace cv;
using namespace Freenect;

int main() {
  Freenect::Freenect freenect;
  MyFreenectDevice& device = freenect.createDevice<MyFreenectDevice>(0);
  device.startVideo();
  device.startDepth();
  resetKalman();

  Mat oldDepthMat, depthMat;
  Mat RGB, rgb, oldRgb, depth, oldDepth, gray, oldGray, depthGray, oldDepthGray;

  dt = get_dt();
  device.updateState();
  THETA = device.getState().getTiltDegs();

  printf("Kinect at an angle of %d Degrees\n", (int)THETA);
  THETA = 3.14159*THETA/180;	// Convert angle in Rads for use.

  Mat depthMatPal(Size(640,480),CV_16UC1);

  vector<KeyPoint> KP;
  vector<Point> pts;
  vector<int> score;

  int minScore;

  SimpleBlobDetector::Params params;
  params.minDistBetweenBlobs = 1.0f;
  params.filterByInertia = false;
  params.minInertiaRatio = 0.0f;
  params.maxInertiaRatio = 1.0f;
  params.minRepeatability = 2;
  params.filterByConvexity = false;
  params.filterByColor = false;
  params.blobColor = 100;
  params.filterByCircularity = false;
  params.minCircularity = 0.9f;
  params.maxCircularity = 1.0f;
  params.filterByArea = true;
  params.minArea = 5.0f;
  params.maxArea = 300.0f;
  Ptr<FeatureDetector> blobDetector = new SimpleBlobDetector(params);

  // wasItDebug defined in debug.h and debug.cpp

  do {
    device.getDepth(oldDepthMat);
    device.getVideo(oldRgb);
  } while (oldRgb.empty() || oldDepthMat.empty());
  align(oldRgb, oldRgb);

  cvtColor(oldRgb, oldGray, COLOR_BGR2GRAY);

  while (true) {
    xInput = 0;
    yInput = 0;
    stringstream joy;
    if(isControl) {
      processInput();
    }
    jp[0] = js.joystickPosition(0);					// Querying the joystick position at the top of each loop.
    jp[1] = js.joystickPosition(1);

    joy << jp[0].x << "," << jp[0].y;
    getJoyMotion();

    int i=0;
    do {
      device.getDepth(depthMat);
      device.getVideo(rgb);
    } while (rgb.empty() || depthMat.empty());
    align(rgb, rgb);

    depthMatPal = depthMat.clone();

    cvtColor(rgb, gray, COLOR_BGR2GRAY);
    blobDetector->detect(rgb, KP);
    for (uint i=0; i<KP.size(); i++) {
      pts.push_back(KP[i].pt);
      score.push_back(0);
    }

    KP.clear();
    RGB = rgb.clone();

    cannyHoughScore(gray, &pts, &score);
    clusterScore(&pts, &score);
    minScore = 0;
    clean(&pts, &score, minScore, RGB, false);
    colorScore(gray, &pts, &score);
    motionScore(gray, oldGray, &pts, &score);
    motionScore(depthMat, oldDepthMat, &pts, &score);
    minScore = 20;
    clean(&pts, &score, minScore, RGB, false);
    if (pts.size() > 0)
      optFlowScore(rgb, oldRgb, &pts, &score);
    minScore = 20;
    clean(&pts, &score, -1, RGB, true);


    stringstream debugger;
    debugger << debug?"Dead":"Live";
    putText(RGB, debugger.str(), Point(270,30), 3, 1, Scalar(255,0,255), 2, 8);
    //==============================Controller=================================//
    if(isControl)
      putText(RGB, joy.str(), Point(280,430), 1, 2, Scalar(0,255,255), 2, 2, 0);
    printFPS(RGB);

    //================================Kalman==================================//

    int left_field = 20;
    int right_field = 620;
    int up_field = 20;
    int down_field = 460;

    line(RGB, Point(left_field, 1),  
	 Point(left_field, 	479), 	Scalar(100, 255, 150), 2, 8, 0);
		
    line(RGB, Point(right_field, 1), 
	 Point(right_field, 	479), 	Scalar(100, 255, 150), 2, 8, 0);

    line(RGB, Point(1, up_field),    
	 Point(639, 	   up_field), 	Scalar(100, 255, 150), 2, 8, 0);

    line(RGB, Point(1, down_field),  
	 Point(639,   down_field), 	Scalar(100, 255, 150), 2, 8, 0);

    int min = 999999;
    int min_x = 999999;
    int min_y = 999999;
    int alpha, beta, gamma;

    for (gamma = 0; gamma < pts.size(); gamma++) {
      for(alpha = maxi(pts[gamma].x-5, left_field); alpha<mini(right_field, pts[gamma].x + 5); alpha++){
	for(beta= maxi(up_field , pts[gamma].y-5); beta<mini(down_field, pts[gamma].y+5); beta++){
	  int pDepth = depthMatPal.at<uint16_t>(beta, alpha);
	  if (pDepth < min){
	    min = pDepth;
	    min_x = alpha;
	    min_y = beta;
	  }	
	}
      }
    }

    pts.clear();
    score.clear();


    z = ((-1)* 7.5 / ( tanF(0.0002157 * min  - 0.2356)))/100.0;
    x = -(z * (min_x - 320) * (-1) / focal)/100.0;
    y = (z * (min_y - 240) * (-1) / focal)/100.0;
    double z_temp = z*cos(THETA) - y*sin(THETA);
    double y_temp = z*sin(THETA) + y*cos(THETA);
    z = z_temp;							
    y = y_temp;							// x, y, z found wrt kinect

    if(z>4 || debug) {
      if(z>4)
	circle(RGB, Point(min_x, min_y), 20, Scalar(255, 0, 0), 5);
      else
	circle(RGB, Point(min_x, min_y), 20, Scalar(255,255,0), 5);
      imshow("Kinect", RGB);
      char ch = (char)waitKey(1);

      switch(ch) {
      case 'r':
	resetKalman();
	waitKey(0);
	break;
      case 'e':
	if(isFile){
	  file = fopen(ARD, "w");
	  fprintf(file , STAY);
	  fclose(file);
	}
	return 0;
	break;
      case 'w':
	if(isFile){
	  file = fopen(ARD, "w");
	  fprintf(file , FRONT_210);
	  fprintf(file , HR_STOP);
	  yInput = 1;
	  fclose(file);
	}
	break;
      case 's':
	if(isFile){
	  file = fopen(ARD, "w");
	  fprintf(file , BACK_210);
	  fprintf(file , HR_STOP);
	  yInput = 1;
	  fclose(file);
	}
	break;
      case 'a':
	if(isFile){
	  file = fopen(ARD, "w");
	  fprintf(file , LEFT_210);
	  fprintf(file , VR_STOP);
	  xInput = 1;
	  fclose(file);
	}
	break;
      case 'd':
	if(isFile){
	  file = fopen(ARD, "w");
	  fprintf(file , RIGHT_210);
	  fprintf(file , VR_STOP);
	  xInput = 1;
	  fclose(file);
	}
	break;
      case 'p':
	if(isFile){
	  file = fopen(ARD, "w");
	  fprintf(file, STAY);
	  fclose(file);
	}
	while(waitKey(0)!='p');
	break;
      case ' ':
	cout<< "----------------------------------------\n";
	break;
      case 't':
	if(isFile && !debug){
	  file = fopen(ARD, "w");
	  fprintf(file, STAY);
	  fclose(file);
	}
	debug = !debug;
	cout << "-------------------------\n";
	if(!debug)
	  resetKalman();
	cout << "Debug mode is " << debug << endl;
	break;
      default:
	if(isFile){
	  file = fopen(ARD, "w");
	  fprintf(file, PAUSE);
	  fclose(file);
	}
	break;
      }
      continue;					// As this case if for when there is no object detected in range.
    }

    //------------MAIN KALMAN CODE-----------//

    // Calculate velocity from coordinates.
    v_x = (x - old_x)/dt;
    v_y = (y - old_y)/dt;
    v_z = (z - old_z)/dt;

    if((abs(v_x>30) || abs(v_y)>30 || abs(v_z)>30 || abs(old_x - 999.999) < 0.01) && !Kalman_flag){
      old_x = x;
      old_y = y;
      old_z = z;
      imshow("Kinect", RGB);
      waitKey(5);
      continue;
    }
    else if(abs(v_x>30) || abs(v_y)>30 || abs(v_z)>30 || abs(old_x - 999.999) < 0.01)
      {
	U_y 	<<	-0.5*g_y*dt*dt 		<< 	arma::endr
		<<	    -g_y*dt 		<<	arma::endr;
	A_x		<<	1 	<<	(dt-k*dt*dt/(2*m))	<<	arma::endr	
			<<	0 	<< 	(1 - k*dt/m) 		<<	arma::endr;
	A_y		<<	1 	<<	(dt-k*dt*dt/(2*m))	<<	arma::endr	
			<<	0 	<< 	(1 - k*dt/m) 		<<	arma::endr;
	A_z		<<	1 	<<	(dt-k*dt*dt/(2*m))	<<	arma::endr	
			<<	0 	<< 	(1 - k*dt/m) 		<<	arma::endr;
	arma::mat xPred_x = A_x * X_x + B_x * U_x;
	arma::mat xPred_y = A_y * X_y + B_y * U_y;
	arma::mat xPred_z = A_z * X_z + B_z * U_z;
	x 	= xPred_x(0);
	v_x = xPred_x(1);
	y 	= xPred_y(0);
	v_y = xPred_y(1);
	z 	= xPred_z(0);
	v_z = xPred_z(1); 
      }

    old_x = x;
    old_y = y;
    old_z = z;
		
    if(!v_x && !v_y && !v_z)
      {
	imshow("Kinect", RGB);
	waitKey(5);
	continue;
      }
    Kalman_flag = true;

    Kalman_calc(x , v_x, y , v_y, z , v_z);
    circle(RGB, Point(min_x, min_y), 20, Scalar(0, 0, 255), 5);
    imshow("Kinect", RGB);

    predict();

    char ch =(char) waitKey(1);
    switch(ch){
    case 'r':
      resetKalman();
      waitKey(0);
      break;
    case 'e':
      if(isFile){
	file = fopen(ARD, "w");
	fprintf(file , STAY);
	fclose(file);
      }
      return 0;
      break;
    case 'w':
      if(isFile){
	file = fopen(ARD, "w");
	fprintf(file , FRONT_210);
	fprintf(file , HR_STOP);
	yInput = 1;
	fclose(file);
      }
      break;
    case 's':
      if(isFile) {
	file = fopen(ARD, "w");
	fprintf(file , BACK_210);
	fprintf(file , HR_STOP);
	yInput = 1;
	fclose(file);
      }
      break;
    case 'a':
      if(isFile){
	file = fopen(ARD, "w");
	fprintf(file , LEFT_210);
	fprintf(file , VR_STOP);
	xInput = 1;
	fclose(file);
      }
      break;
    case 'd':
      if(isFile) {
	file = fopen(ARD, "w");
	fprintf(file , RIGHT_210);
	fprintf(file , VR_STOP);
	xInput = 1;
	fclose(file);
      }
      break;
    case 'p':
      if(isFile) {
	file = fopen(ARD, "w");
	fprintf(file, STAY);
	fclose(file);
      }
      while(waitKey(0)!='p');
      break;
    case ' ':
      cout<< "----------------------------------------\n";
      break;
    case 't':
      if(isFile && !debug){
	file = fopen(ARD, "w");
	fprintf(file, STAY);
	fclose(file);
      }
      debug = !debug;
      cout << "-------------------------\n";
      if(!debug)
	resetKalman();
      cout << "Debug mode is " << debug << endl;
      break;
    default:
      if(isFile){
	file = fopen(ARD, "w");
	fprintf(file, PAUSE);
	fclose(file);
      }
      break;
    }				// Switch case closing bracket.


    imshow("Kinect", RGB);
    oldRgb = rgb.clone();
    oldGray = gray.clone();
    oldDepthMat = depthMat.clone();
  }


  if(isFile){
    file = fopen(ARD, "w");
    fprintf(file , HR_STOP);
    fprintf(file , VR_STOP);
    fprintf(file , STAY);
    fclose(file);
  }

  device.stopVideo();
  device.stopDepth();
  destroyAllWindows();
  return 0;
}
