#include "../include/optflow.h"

void optFlowScore(Mat prevImg, Mat img, vector<Point> *points, vector<int> *score) {
  int optFlow_score = 10;
  int optFlow_penalty = 0;


  if (points->size() > 0) {
    vector<Point2f> featuresPrev, featuresNext;
    vector<uchar> status;
    vector<float> err;
    
    for (int i=0; i<points->size(); i++) {
      featuresNext.push_back(points->at(i));
    }
    
    calcOpticalFlowPyrLK(img, prevImg, featuresNext, featuresPrev, status, err);
    
    float maxDist = 0;
    int maxLoc;
    float x, y;
    
    for (int i=0; i<points->size(); i++) {
      x = featuresNext[i].x - featuresPrev[i].x;
      y = featuresNext[i].y - featuresPrev[i].y;
      if (x*x + y*y > maxDist) {
	maxDist = x*x + y*y;
	maxLoc = i;
      }
    }
    score->at(maxLoc) += optFlow_score;
  }
}


// max = 0;
// for (uint i=0; i<pts.size(); i++) {
//   featuresNext.push_back(pts[i]);
// }

// Mat optFlow;
// optFlow = Mat::zeros(gray.size(), CV_8UC1);

// calcOpticalFlowPyrLK(rgb, oldRgb, featuresNext, featuresPrev, status, err);

// for (uint i=0; i<pts.size(); i++) {
//   pt = featuresNext[i] - featuresPrev[i];
//   if (pt.x * pt.x + pt.y * pt.y > max) {
//     max = pt.x * pt.x + pt.y * pt.y;
//     index = i;
//   }
// }   

// score[index] += optFlow_score;
// featuresNext.clear();
// featuresPrev.clear();
