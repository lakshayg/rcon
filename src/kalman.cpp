#include "../include/kalman.h"

float k   = 0.003;
float m   = 0.005;		//	SI UNITS
float g_x = 0;
float g_y = 9.80665;
float g_z = 0;
float dt  = 0.04;

double x = 0;
double y = 0;
double z = 0;
double v_x  = 0;
double v_y  = 0;
double v_z  = 0;
double old_x  = 999.999;
double old_y  = 999.999;
double old_z  = 999.999;
bool Kalman_flag = 0;

arma::mat A_x = arma::mat(2,2,arma::fill::eye);
arma::mat P_x = arma::mat(2,2,arma::fill::eye);
arma::mat Q_x = arma::mat(2,2,arma::fill::eye);
arma::mat B_x = arma::mat(2,2,arma::fill::eye);
arma::mat H_x = arma::mat(2,2,arma::fill::eye);
arma::mat R_x = arma::mat(2,2,arma::fill::eye);
arma::mat X_x = arma::mat(2,1,arma::fill::zeros);
arma::mat U_x = arma::mat(2,1,arma::fill::zeros); 

arma::mat A_y = arma::mat(2,2,arma::fill::eye);
arma::mat P_y = arma::mat(2,2,arma::fill::eye);
arma::mat Q_y = arma::mat(2,2,arma::fill::eye);
arma::mat B_y = arma::mat(2,2,arma::fill::eye);
arma::mat H_y = arma::mat(2,2,arma::fill::eye);
arma::mat R_y = arma::mat(2,2,arma::fill::eye);
arma::mat X_y = arma::mat(2,1,arma::fill::zeros);
arma::mat U_y = arma::mat(2,1,arma::fill::zeros); 

arma::mat A_z = arma::mat(2,2,arma::fill::eye);
arma::mat P_z = arma::mat(2,2,arma::fill::eye);
arma::mat Q_z = arma::mat(2,2,arma::fill::eye);
arma::mat B_z = arma::mat(2,2,arma::fill::eye);
arma::mat H_z = arma::mat(2,2,arma::fill::eye);
arma::mat R_z = arma::mat(2,2,arma::fill::eye);
arma::mat X_z = arma::mat(2,1,arma::fill::zeros);
arma::mat U_z = arma::mat(2,1,arma::fill::zeros); 

void Kalman_calc(double x, double v_x, double y, double v_y, double z, double v_z) {
  arma::mat Z_x;
  arma::mat xPred_x;
  arma::mat pPred_x;
  arma::mat y_x;
  arma::mat S_x;
  arma::mat K_x;
  arma::mat I_x = arma::mat(2,2,arma::fill::eye);

  Z_x << x << arma::endr << v_x << arma::endr;

  xPred_x = A_x*X_x + B_x*U_x;
  pPred_x = A_x*P_x*arma::trans(A_x) + Q_x;
  y_x 	  = Z_x - H_x*xPred_x;
  S_x 	  = H_x*pPred_x*arma::trans(H_x) + R_x;
  K_x 	  = pPred_x*arma::trans(H_x)*arma::inv(S_x);
  X_x 	  = xPred_x + K_x*y_x;
  P_x 	  = (I_x - K_x*H_x)*pPred_x;


  arma::mat Z_y;
  arma::mat xPred_y;
  arma::mat pPred_y;
  arma::mat y_y;
  arma::mat S_y;
  arma::mat K_y;
  arma::mat I_y = arma::mat(2,2,arma::fill::eye);

  Z_y << y << arma::endr << v_y << arma::endr;

  xPred_y = A_y*X_y + B_y*U_y;
  pPred_y = A_y*P_y*arma::trans(A_y) + Q_y;
  y_y 	  = Z_y - H_y*xPred_y;
  S_y 	  = H_y*pPred_y*arma::trans(H_y) + R_y;
  K_y 	  = pPred_y*arma::trans(H_y)*arma::inv(S_y);
  X_y 	  = xPred_y + K_y*y_y;
  P_y 	  = (I_y - K_y*H_y)*pPred_y;


  arma::mat Z_z;
  arma::mat xPred_z;
  arma::mat pPred_z;
  arma::mat y_z;
  arma::mat S_z;
  arma::mat K_z;
  arma::mat I_z = arma::mat(2,2,arma::fill::eye);

  Z_z << z << arma::endr << v_z << arma::endr;

  xPred_z = A_z*X_z + B_z*U_z;
  pPred_z = A_z*P_z*arma::trans(A_z) + Q_z;
  y_z 	  = Z_z - H_z*xPred_z;
  S_z 	  = H_z*pPred_z*arma::trans(H_z) + R_z;
  K_z 	  = pPred_z*arma::trans(H_z)*arma::inv(S_z);
  X_z 	  = xPred_z + K_z*y_z;
  P_z 	  = (I_z - K_z*H_z)*pPred_z;
}

void resetKalman() {
  reInitKalman(x);
  reInitKalman(y);
  reInitKalman(z);
  x 	= 0;
  y 	= 0;
  z 	= 0;
  v_x  = 0;
  v_y  = 0;
  v_z  = 0;
  old_x  = 999.999;
  old_y  = 999.999;
  old_z  = 999.999;
  Kalman_flag = 0;
}

void predict() {
  dt = get_dt();
  //  cout << dt << endl;
  int t=50;
  if(isFile) file = fopen(ARD, "w");
  arma::mat Prediction_x = A_x*X_x + B_x*U_x;
  arma::mat Prediction_y = A_y*X_y + B_y*U_y;
  arma::mat Prediction_z = A_z*X_z + B_z*U_z;
  while(t--) {
    if(Prediction_y(0, 0) < 0.05) /* && Prediction_y(1, 0) < 0) */ {
      if(Prediction_x(0, 0) > 0.1)
	{
	  printf("Right\t%lf\n" , Prediction_x(0, 0)*100);
	  if(isFile && !debug && !xInput) fprintf(file, RIGHT_KIN);
	}
      else if(Prediction_x(0, 0) < -0.1)
	{
	  printf("Left\t%lf\n" , Prediction_x(0, 0)*100);
	  if(isFile && !debug && !xInput) fprintf(file, LEFT_KIN);
	}
      else 
	{
	  printf("Stop\t%lf\n" , Prediction_x(0, 0)*100);
	  if(isFile && !debug && !xInput) fprintf(file, HR_STOP);
	}
      if(Prediction_z(0, 0) > 0.1)
	{
	  printf("Front\t%lf\n" , Prediction_z(0, 0)*100);
	  if(isFile && !debug && !yInput) fprintf(file, FRONT_KIN);
	}
      else if(Prediction_z(0, 0) < -0.1)
	{
	  printf("Back\t%lf\n" , Prediction_z(0, 0)*100);
	  if(isFile && !debug && !yInput) fprintf(file, BACK_KIN);
	}
      else
	{
	  printf("Stay\t%lf\n" , Prediction_z(0, 0)*100);
	  if(isFile && !debug && !yInput) fprintf(file, VR_STOP);
	}
      break;
    }
    Prediction_x = A_x*Prediction_x + B_x*U_x;
    Prediction_y = A_y*Prediction_y + B_y*U_y;
    Prediction_z = A_z*Prediction_z + B_z*U_z;
  }
  if(isFile) fclose(file);
}

int mini(int a, int b) {
  return std::min(a, b);
}

int maxi(int a, int b) {
  return std::max(a, b);
}

double tanF(double x) {
  return (x-x*x*x/3);
}

double get_dt() {
  // Returns the time between two frames
  double t2 = cv::getTickCount();
  double freq = cv::getTickFrequency();
  static double t1 = t2 - freq*0.04;
  double dt = ((t2-t1)/freq);
  t1 = t2;
  return dt;
}
