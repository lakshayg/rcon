#include "../include/canny_hough.h"

void cannyHoughScore(Mat gray, vector<Point> *points, vector<int> *score) {

  int canny_score = 0;
  int canny_penalty = 10;
  int hough_score = 0;
  int hough_penalty = 50;

  Mat dst, cdst;
  int roi_size = 10;
  Canny(gray, dst, 100, 200, 3);
  cdst = Mat::zeros(dst.size(), CV_8UC1);
  Rect U = Rect(Point(0,0), Size(640, 480));
  Rect roi;
  int x, y;
  
  vector<Vec4i> lines;
  HoughLinesP(dst, lines, 1, CV_PI/180, 50, 50, 10 );
  for( size_t i = 0; i < lines.size(); i++ ) {
    Vec4i l = lines[i];
    line( cdst, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255,255,255), 3, CV_AA);
  }
  
  for (int i=0; i<points->size(); i++) {
    x = points->at(i).x;
    y = points->at(i).y;
    roi = Rect(Point(x - roi_size, y - roi_size), Size(2*roi_size+1, 2*roi_size+1)) & U;
    
    score->at(i) += (countNonZero(dst(roi)) > 0 ? canny_score : -canny_penalty);
    score->at(i) += (countNonZero(cdst(roi)) > 0 ? -hough_penalty : hough_score);
  }  

  // imshow("canny", dst);
  // imshow("hough", cdst);

}
