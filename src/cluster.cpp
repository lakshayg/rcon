#include "../include/cluster.h"

void clusterScore(vector<Point> *keypoints, vector<int> *score) {
  float eps = 30;
  int minPts = 3;
  vector<bool> clustered(keypoints->size(), false), visited(keypoints->size(), false);
  vector<int> neighborPts;
  Point pt;  
  int noKeys = keypoints->size();
  
  for(int i = 0; i < noKeys; i++) {
    if(!visited[i]) {
      visited[i] = true;
      for (int j=0; j<noKeys; j++) {
	pt = keypoints->at(i) - keypoints->at(j);
	if (pt.x * pt.x + pt.y * pt.y < eps*eps)
	  neighborPts.push_back(j);
      }
      if(neighborPts.size() >= minPts)
	for(int j = 0; j < neighborPts.size(); j++)
	  if(!visited[neighborPts[j]]) {
	    visited[neighborPts[j]] = true;
	    clustered[neighborPts[j]] = true;
	  }
      neighborPts.clear();
    }
  }
  
  for (int i=0; i<noKeys; i++)
    score->at(i) += (clustered[i] ? -cluster_penalty : cluster_score);
}
