#include "../include/align.h"

void align(InputArray in, OutputArray out) {
  Mat input;
  input = in.getMat();

  Mat output = Mat::zeros(input.size(), CV_8UC3);
  int rgb_channels = in.channels();
  int ax, ay, val;
  for(int i=0; i<640; i++) {
    for(int j=0; j<480; j++) {
      for(int k=0; k<rgb_channels; k++) {
	ax = 9.1008894 + 0.924429*i;
	ay = 40.0500822 + 0.92345789*j;
	if(ax<0 || ay<0 || ax>640 || ay>480)
	  val=0;
	else
	  val = input.at<uchar>(ay,ax*rgb_channels+k);
	output.at<uchar>(j, i*rgb_channels+k) = val;
      }
    }
  }
  output.copyTo(out);
}
