#include "../include/motion.h"

void motionScore(Mat prevImg, Mat img, vector<Point> *candidates, vector<int> *score) {
  Mat diff;
  int roi_size = 10;
  Point droi = Point(roi_size, roi_size);
  Size sroi = Size(2*roi_size+1, 2*roi_size+1);
  Rect roi, U = Rect(Point(0,0), Size(640,480));
  Mat dilateElement = getStructuringElement(MORPH_RECT, Size(8,8));

  absdiff(img, prevImg, diff);

  if (img.type() == CV_8UC1) {
    threshold(diff, diff, 50, 255, THRESH_BINARY);
    dilate(diff, diff, dilateElement, Point(-1,-1), 1);
    //imshow("rgb motion", diff);
  } else if (img.type() == CV_16UC1) {
    diff.convertTo(diff, CV_8UC1, 255.0/2047.0);
    threshold(diff, diff, 50, 255, THRESH_BINARY);
    //imshow("depth motion", diff);
  }

  for (uint i=0; i<candidates->size(); i++) {
    roi = Rect(candidates->at(i)-droi, sroi) & U;
    score->at(i) +=  (countNonZero(diff(roi)) > 0 ? motion_score : 0);
  }
}
