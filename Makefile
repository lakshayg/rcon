FILTERS = bin/cluster.o bin/canny_hough.o bin/motion.o bin/optflow.o bin/color.o
OBJECTS = bin/glhelper.o bin/util.o bin/kinect.o bin/main.o bin/kalman.o bin/debug.o bin/controller.o bin/clean.o bin/align.o $(FILTERS)
LDFLAGS = -lpthread -lfreenect -larmadillo `pkg-config --libs opencv` -lGL -lGLU -lglut `sdl-config --cflags --libs` -lSDL_ttf -lSDL_image
LDDIR   = -I/usr/local/include/libfreenect -I/usr/include/eigen3

main: $(OBJECTS)
	g++ $(OBJECTS) -o main $(LDDIR) $(LDFLAGS)

bin/main.o: src/main.cpp
	g++ -c src/main.cpp -o bin/main.o -I /usr/include/libusb-1.0/ -w

bin/kinect.o: src/kinect.cpp
	g++ -c src/kinect.cpp -o bin/kinect.o -I /usr/include/libusb-1.0/

bin/kalman.o: src/kalman.cpp
	g++ -c src/kalman.cpp -o bin/kalman.o -w

bin/%.o: src/%.cpp
	g++ -c $? -o $@

clean:
	rm -rf bin/* main
